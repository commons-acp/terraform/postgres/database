# PostgreSQL module

It creates schema in db and read_write and read_only users which have access to this schema.

You have to specify provider in order to use this module. Example:
```
provider "postgresql" {
  host            = "dbhost.examlple.com"
  port            = 5432
  database        = "postgres"
  username        = "postgres"
  password        = "SecretPassword"
  sslmode         = "require"
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| allow\_connections | Allow connection | `bool` | `true` | no |
| connection\_limit | Connection limit | `number` | `-1` | no |
| create\_database | If true then database will be created. | `bool` | `true` | no |
| create\_read\_only\_role | If true then read\_only\_role will be created. | `bool` | `true` | no |
| create\_read\_write\_role | If true then read\_write\_role will be created. | `bool` | `true` | no |
| create\_schema | If true then schema will be created. | `bool` | `true` | no |
| database | Database to connect to | `string` | `"postgres"` | no |
| db\_name | Name of databse | `string` | n/a | yes |
| host | Name of a host to connect to or IP address | `string` | n/a | yes |
| lc\_collate | LC\_COLLATE | `string` | `"DEFAULT"` | no |
| owner | Owner of database | `string` | `"root"` | no |
| password | Password to use for connection | `string` | n/a | yes |
| port | Port to connect to | `number` | `5432` | no |
| read\_only\_role\_name | The name of the read\_only\_role | `string` | `"read_only"` | no |
| read\_only\_role\_password | The password of the read\_only\_role | `string` | `""` | no |
| read\_write\_role\_name | The name of the read\_write\_role | `string` | `"read_write"` | no |
| read\_write\_role\_password | The password of the read\_write\_role | `string` | `""` | no |
| schema\_name | The name of the schema. Must be unique in the PostgreSQL database instance where it is configured. | `string` | n/a | yes |
| sslmode | SSL mode type. One of disable/require/verify-ca/verify-full | `string` | `"require"` | no |
| template | Template for databse to use | `string` | `"DEFAULT"` | no |
| username | User to use for connection | `string` | `"postgres"` | no |

## Outputs

No output.

