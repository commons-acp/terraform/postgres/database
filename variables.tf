variable "host" {
  description = "host vm databases"
}
variable "port" {
  description = "port postgres"
}
variable "username" {
  description = "username postgres"
}
variable "password" {
  description = "password postgres"
}
variable "sslmode" {
  description = "sslmode"
}
variable "name_database" {
  description = "name of database"
}